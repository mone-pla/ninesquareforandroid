package monepla.co.jp.ninesquea.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.recyclerview.widget.RecyclerView

import java.util.ArrayList

import monepla.co.jp.ninesquea.common.EditEvent
import monepla.co.jp.ninesquea.common.LogFnc
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.R


class NineAdapter(context: Context, private val listener: EditEvent.EventClickListener) : RecyclerView.Adapter<NineViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var nineList: List<Nine> = ArrayList()

    /**
     * Called when RecyclerView needs a new [NineViewHolder] of the given type to represent
     * an item.
     *
     *
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     *
     *
     * The new ViewHolder will be used to display items of the adapter using
     * [.onBindViewHolder]. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary [View.findViewById] calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see .getItemViewType
     * @see .onBindViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NineViewHolder {
        return NineViewHolder(mInflater.inflate(R.layout.nine_layout, parent, false))
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the [NineViewHolder.itemView] to reflect the item at the given
     * position.
     *
     *
     * Note that unlike [ListView], RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the `position` parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use [NineViewHolder.getAdapterPosition] which will
     * have the updated adapter position.
     *
     *
     * Override [.onBindViewHolder] instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: NineViewHolder, position: Int) {
        LogFnc.logTraceStart()
        holder.nameTextView.text = nineList[position].name
        holder.nineTextView.text = nineList[position].objectId
        val event = EditEvent(position)
        event.setListener(listener)
        holder.nameTextView.setOnClickListener(event)
        holder.nameTextView.setOnLongClickListener(event)
        LogFnc.logTraceEnd()
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    override fun getItemCount(): Int {
        return nineList.size
    }

    fun setNineList(nineList: List<Nine>) {
        this.nineList = nineList
        notifyDataSetChanged()
    }

    fun getNine(position: Int): Nine {
        return nineList[position]
    }
}
