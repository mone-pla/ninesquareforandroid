package monepla.co.jp.ninesquea.viewModel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import monepla.co.jp.ninesquea.model.Detail
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.repository.DetailRepository
import monepla.co.jp.ninesquea.repository.NineRepository
import java.util.*

class NineNineViewModel : ViewModel() {
    private val nineRepository = NineRepository()
    private val detailRepository = DetailRepository()
    private var nineMutableLiveData = MutableLiveData<Nine>()
    private var detailListMutableLiveData = MutableLiveData<List<Detail>>()
    private var detailMutableLiveData = MutableLiveData<Detail>()

    fun getNine(id: String): LiveData<Nine> {
        nineRepository.getItem(id).addSnapshotListener { doc, e ->
            if (e != null || doc == null) {
                nineMutableLiveData = MutableLiveData()
                return@addSnapshotListener
            }
            nineMutableLiveData.setValue(doc.toObject(Nine::class.java)!!.withId(doc.id))
        }
        return nineMutableLiveData
    }

    fun getDetailList(id: String): LiveData<List<Detail>> {
        detailRepository.collection.whereEqualTo("nineId", id).addSnapshotListener { queryDocumentSnapshots, e ->
            if (e != null || queryDocumentSnapshots == null) {
                detailListMutableLiveData = MutableLiveData()
                return@addSnapshotListener
            }
            val detailList = ArrayList<Detail>()
            for (doc in queryDocumentSnapshots.documents) {
                val detail = doc.toObject(Detail::class.java)!!.withId<Detail>(doc.id)
                detailList.add(detail)
            }
            detailListMutableLiveData.setValue(detailList)
        }

        return detailListMutableLiveData
    }

    fun getDetail(id: String): LiveData<Detail> {
        detailRepository.collection.document(id).addSnapshotListener { doc, e ->
            if (e != null || doc == null) {
                detailMutableLiveData = MutableLiveData()
                return@addSnapshotListener
            }
            detailMutableLiveData.setValue(doc.toObject(Detail::class.java)!!.withId(doc.id))
        }
        return detailMutableLiveData
    }
}
