package monepla.co.jp.ninesquea.common

/**
 * Created by user on 2017/01/01.
 */

class Event internal constructor() {
    var eventId: Int = 0
    var position: Int = 0
    var text: String? = null
}
