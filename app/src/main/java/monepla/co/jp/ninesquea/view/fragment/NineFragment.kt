package monepla.co.jp.ninesquea.view.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.Switch
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import butterknife.ButterKnife
import com.github.amlcurran.showcaseview.ShowcaseView
import com.github.amlcurran.showcaseview.targets.ViewTarget
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.firebase.analytics.FirebaseAnalytics
import monepla.co.jp.ninesquea.R
import monepla.co.jp.ninesquea.common.*
import monepla.co.jp.ninesquea.common.Common.ids
import monepla.co.jp.ninesquea.model.Detail
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.view.CellView
import monepla.co.jp.ninesquea.view.fragment.NineFragmentDirections.*
import monepla.co.jp.ninesquea.viewModel.DetailViewModel


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the [NineFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NineFragment : Fragment(), DialogFragmentListener {
    private val cellViews = arrayOfNulls<CellView>(9)
    private var nine: Nine? = null
    private var details: List<Detail> = ArrayList()
    private var detail = Detail()
    private var textDialog: EditBottomDialog? = null
    private var switchAction: Switch? = null
    private var closeAction:MenuItem? = null
    private var showcaseView: ShowcaseView? = null
    private var mAdView: AdView? = null
    private var viewModel: DetailViewModel? = null
    private val args: NineFragmentArgs by navArgs()


    private val cellEventListener = object : CellView.CellEventListener {
        override fun textClick(v: View) {
            LogFnc.logTraceStart()
            if (switchAction!!.isChecked) {
                editText(v)
                return
            }
            if (listOf(*ids)[4] == v.id) centerMove()
            if (!TextUtils.isEmpty(cellViews[listOf(*ids).indexOf(v.id)]!!.text)) roundMove(v)
            LogFnc.logTraceEnd()
        }
    }

    private val nextClickListener = View.OnClickListener {
        showcaseView!!.hide()
        ShowcaseView.Builder(activity!!)
                .setTarget(ViewTarget(cellViews[0]))
                .setContentTitle(getString(R.string.goal))
                .setContentText(getString(R.string.tutorial_next))
                .withMaterialShowcase()
                .hideOnTouchOutside()
                .build()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_nine, container, false)
        ButterKnife.bind(this, view)
        setHasOptionsMenu(true)
        MobileAds.initialize(context!!)
        mAdView = view.findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView!!.loadAd(adRequest)
        if (Common.getSharePreferenceInt(activity!!, Common.PREF_BUY, Common.PREF_BUY) == 1) mAdView!!.visibility = View.GONE
        else mAdView!!.visibility =View.VISIBLE

                return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        /*
         * add( int groupId,
         *  int itemId,
         *  int order,
         *  int titleRes //title string or resource identifier )
         */
        switchAction = menu.findItem(R.id.action_edit).actionView as Switch
        switchAction!!.isChecked = true
        menu.getItem(0).isVisible = true

        closeAction = menu.findItem(R.id.action_home)
        menu.getItem(1).isVisible = true
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     *
     * Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     *
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     *
     * @see .onCreateOptionsMenu
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_home) Navigation.findNavController(view!!).navigate(backHome())
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        LogFnc.logTraceStart()
        setEvents(view)
        if (Common.getSharePreferenceInt(activity!!, Common.PREF_BUY, Common.PREF_BUY) == 1) mAdView!!.visibility = View.GONE
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(activity!!.application).create(DetailViewModel::class.java)
        if (TextUtils.isEmpty(args.nine)) {
            newNine()
            return
        }
        loadNine(args.nine)
        loadDetail(args.nine)


        if (!TextUtils.isEmpty(args.detail)) {
            viewModel!!.getDetail(args.detail).observe(this, Observer{ detail ->
                cellViews[4]!!.text = detail.name!!
                this.detail = detail
            })
        }

        LogFnc.logTraceEnd()
    }

    private fun newNine() {
        nine = Nine()
        textDialog = EditBottomDialog.newInstance()
        textDialog!!.setListener(4, this)
        textDialog!!.setPosition(4)
        textDialog!!.setTitle(R.string.new_content)
        textDialog!!.show(fragmentManager!!)
    }

    private fun setEvents(view: View?) {
        for (i in ids.indices) {
            cellViews[i] = view!!.findViewById(ids[i])
            cellViews[i]!!.setCellEventListener(cellEventListener)
        }
    }

    /**
     * On event.
     *
     * @param id    listener id
     * @param event event is either of the following. [.ON_DISMISS] [.ON_CANCEL],
     * [.ON_POSITIVE_BUTTON_CLICKED], [.ON_NEGATIVE_BUTTON_CLICKED],
     * [.ON_CLOSE_BUTTON_CLICKED].
     */
    override fun onEvent(id: Int, event: Event) {
        LogFnc.logTraceStart()
        LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.LEVEL_UP,"UPDATE")
        if (event.eventId == DialogFragmentListener.ON_POSITIVE_BUTTON_CLICKED) {
            cellViews[event.position]!!.text = event.text!!
            assert(arguments != null)
            if (event.position == 4 && TextUtils.isEmpty(args.detail))
                centerNine(event)
            else if (event.position == 4)
                centerDetail(event)
            else
                roundDetail(event)
        } else if(event.position == 4 && TextUtils.isEmpty(event.text) && TextUtils.isEmpty(args.nine)){
            Navigation.findNavController(view!!).navigate(backHome())
        }
        LogFnc.logTraceEnd()
    }

    private fun centerNine(event: Event) {
        nine!!.name = event.text
        if (TextUtils.isEmpty(nine!!.objectId)) {
            viewModel!!.createNine(nine!!).addOnCompleteListener { task ->
                if (task.isSuccessful && task.result != null) {
                    loadNine(task.result!!.id)
                }
            }
        } else
            viewModel!!.updateNine(nine!!)

        showcaseView = ShowcaseView.Builder(activity)
                .setTarget(ViewTarget(activity!!.findViewById(R.id.action_edit)))
                .setOnClickListener(nextClickListener)
                .setContentTitle(getString(R.string.goal))
                .setContentText(getString(R.string.tutorial_text))
                .withMaterialShowcase()
                .hideOnTouchOutside()
                .build()
        showcaseView!!.setButtonText("next")
    }

    private fun centerDetail(event: Event) {
        detail.name = event.text
        viewModel!!.updateDetail(detail)
    }

    private fun roundDetail(event: Event) {
        var round = Detail()
        for (d in details) {
            if (d.no == event.position && d.detailId == args.detail) {
                round = d
                break
            }
        }
        if (TextUtils.isEmpty(round.objectId)) {
            round.nineId = nine!!.objectId
            round.detailId = args.detail
            round.no = event.position
        }
        round.name = event.text
        if (TextUtils.isEmpty(round.objectId))
            viewModel!!.createDetail(round).addOnCompleteListener { task ->
                if (TextUtils.isEmpty(args.nine) && details.isEmpty()) {
                    loadDetail(nine!!.objectId)
                }
            }
        else
            viewModel!!.updateDetail(round)
    }


    private fun centerMove() {
        LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.ADD_TO_CART,"MoveNineNineView")
        if (nine == null || TextUtils.isEmpty(nine!!.objectId)) return
//        fragmentManager!!.beginTransaction()
//                .replace(R.id.fragment, NineNineFragment.newInstance(nine!!.objectId!!, args.detail))
//                .addToBackStack(null).commit()
        Navigation.findNavController(view!!).navigate(nextNineNineFragment(nine!!.objectId!!, args.detail))
    }

    private fun roundMove(v: View) {
        LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.SELECT_CONTENT,"MoveNineView")
        if(view == null || nine == null || nine!!.objectId == null) return
        for (moveDetail in details) {
            if (listOf(*ids).indexOf(v.id) == moveDetail.no) {
                Navigation.findNavController(view!!).navigate(nextNineFragment(nine!!.objectId!!, moveDetail.objectId!!))
                return
            }
        }

    }

    private fun editText(v: View) {
        val position = listOf(*ids).indexOf(v.id)
        textDialog = EditBottomDialog.newInstance(cellViews[position]!!.text)
        textDialog!!.setListener(position, this@NineFragment)
        textDialog!!.setPosition(position)
        textDialog!!.setTitle(R.string.change)
        textDialog!!.show(fragmentManager!!)
    }

    private fun loadNine(nineId: String) {
        if(TextUtils.isEmpty(nineId)) return
        viewModel!!.getNine(nineId).observe(this, Observer{ nine ->
            if (TextUtils.isEmpty(args.detail)) {
                cellViews[4]!!.text = nine.name!!
            }
            this.nine = nine
        })
    }

    private fun loadDetail(nineId: String?) {
        viewModel!!.getDetailList(nineId!!, args.detail).observe(this, Observer{ details1 ->
            this.details = details1
            for (detail in details) {
                cellViews[detail.no]!!.text = detail.name!!
            }
        })
    }

    companion object {

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NineFragment.
         */
        fun newInstance(param1: String, param2: String): NineFragment {
            val fragment = NineFragment()
            val args = Bundle()
            args.putString(Common.PARAM_NINE_ID, param1)
            args.putString(Common.PARAM_DETAIL_ID, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor