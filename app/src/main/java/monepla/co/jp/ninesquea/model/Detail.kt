package monepla.co.jp.ninesquea.model

import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table

import monepla.co.jp.ninesquea.common.AppModel


/**
 * Created by user on 2016/12/31.
 */
@Table(name = "Detail")
class Detail : AppModel() {
    @Column(name = "nine_id")
    var nine_id: Long = 0
    @Column(name = "sub_id")
    var sub_id: Long = 0
    @Column(name = "no")
    var no: Int = 0
    @Column(name = "name")
    var name: String? = null
    var nineId: String? = null
    var detailId: String? = null

}
