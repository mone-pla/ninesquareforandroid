package monepla.co.jp.ninesquea.repository

import monepla.co.jp.ninesquea.common.FirebaseRepository
import monepla.co.jp.ninesquea.model.Detail

class DetailRepository : FirebaseRepository<Detail>() {
    override val rootNode: String
        get() = "detail"
}
