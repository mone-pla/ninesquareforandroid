package monepla.co.jp.ninesquea.viewModel

import androidx.lifecycle.ViewModel

import com.activeandroid.query.Select
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.WriteBatch

import monepla.co.jp.ninesquea.common.LoadingListener
import monepla.co.jp.ninesquea.model.Detail
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.repository.DetailRepository
import monepla.co.jp.ninesquea.repository.NineRepository

class AnyOneViewModel : ViewModel() {
    private val nineRepository = NineRepository()
    private val detailRepository = DetailRepository()
    fun addNine(nineList: List<Nine>, loadingListener: LoadingListener) {
        val batch = FirebaseFirestore.getInstance().batch()
        for (nine in nineList) {
            batch.set(nineRepository.collection.document(), nine)
        }
        batch.commit().addOnSuccessListener { getNine(loadingListener) }
    }

    private fun getNine(loadingListener: LoadingListener) {
        nineRepository.collection.get().addOnSuccessListener { task ->
            val batch = FirebaseFirestore.getInstance().batch()
            task.documents.forEach { doc ->
                val nine = doc.toObject(Nine::class.java)!!.withId<Nine>(doc.id)
                doc.getLong("id")
                addDetail(nine, batch,doc.getLong("id"))
            }
            batch.commit().addOnSuccessListener { loadingListener.onCompleter() }
        }
    }

    private fun addDetail(nine: Nine, batch: WriteBatch,id:Long?) {
        val detailList = Select().from(Detail::class.java).where("nine_id=?", id).execute<Detail>()
        for (detail in detailList) {
            detail.nineId = nine.objectId
            if (detail.detailId == null) detail.detailId = ""
            batch.set(detailRepository.collection.document(), detail)
        }
    }
}
