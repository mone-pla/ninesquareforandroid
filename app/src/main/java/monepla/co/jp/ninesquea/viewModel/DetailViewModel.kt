package monepla.co.jp.ninesquea.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import monepla.co.jp.ninesquea.model.Detail
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.repository.DetailRepository
import monepla.co.jp.ninesquea.repository.NineRepository
import java.util.*

class DetailViewModel : ViewModel() {
    private val nineRepository = NineRepository()
    private val detailRepository = DetailRepository()
    private var nineMutableLiveData = MutableLiveData<Nine>()
    private var detailListMutableLiveData = MutableLiveData<List<Detail>>()
    private var detailMutableLiveData = MutableLiveData<Detail>()

    fun getNine(id: String): LiveData<Nine> {
        nineRepository.getItem(id).addSnapshotListener { doc, e ->
            if (e != null || doc == null) {
                nineMutableLiveData = MutableLiveData()
                return@addSnapshotListener
            }
            val nine = doc.toObject(Nine::class.java) ?: return@addSnapshotListener
            nineMutableLiveData.setValue(nine.withId(doc.id))

        }
        return nineMutableLiveData
    }

    fun getDetailList(id: String, detailId: String): LiveData<List<Detail>> {
        detailRepository.collection.whereEqualTo("nineId", id).whereEqualTo("detailId", detailId).addSnapshotListener { queryDocumentSnapshots, e ->
            if (e != null || queryDocumentSnapshots == null) {
                detailListMutableLiveData = MutableLiveData()
                return@addSnapshotListener
            }
            val detailList = ArrayList<Detail>()
            for (doc in queryDocumentSnapshots.documents) {
                val detail: Detail = doc.toObject(Detail::class.java) ?: return@addSnapshotListener
                detailList.add(detail)
            }
            detailListMutableLiveData.setValue(detailList)
        }

        return detailListMutableLiveData
    }

    fun getDetail(id: String): LiveData<Detail> {
        detailRepository.collection.document(id).addSnapshotListener { doc, e ->
            if (e != null || doc == null) {
                detailMutableLiveData = MutableLiveData()
                return@addSnapshotListener
            }
            val detail: Detail = doc.toObject(Detail::class.java) ?: return@addSnapshotListener
            detailMutableLiveData.setValue(detail.withId(doc.id))
        }
        return detailMutableLiveData
    }

    fun createNine(nine: Nine): Task<DocumentReference> {
        return nineRepository.createItem(nine)
    }

    fun updateNine(nine: Nine): Task<Void> {
        return nineRepository.updateItem(nine)
    }

    fun createDetail(detail: Detail): Task<DocumentReference> {
        return detailRepository.createItem(detail)
    }

    fun updateDetail(detail: Detail): Task<Void> {
        return detailRepository.updateItem(detail)
    }
}
