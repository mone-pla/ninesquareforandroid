package monepla.co.jp.ninesquea.common

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView

import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.FragmentManager

import com.google.android.material.bottomsheet.BottomSheetDialogFragment

import monepla.co.jp.ninesquea.R

class EditBottomDialog : BottomSheetDialogFragment(), View.OnClickListener {
    private var listenerId: Int = 0
    private var listener: DialogFragmentListener? = null

    private var clickGuard = false
    private var dismissFlag = false
    private var position: Int = 0

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val layout = View.inflate(context, LAYOUT, null)
        dialog.setContentView(layout)
        dialog.setOnShowListener {
            val editText = dialog.findViewById<AppCompatEditText>(ID_EDIT_TEXT)
            if (editText != null && context != null) {
                val inputMethodManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                editText.setSelection(editText.text!!.length)
                inputMethodManager.showSoftInput(editText, 0)
            }
        }
        LogFnc.logTraceStart()
        val noPosBtn = 0 and NO_POSITIVE_BUTTON != 0
        val noNegBtn = 0 and NO_NEGATIVE_BUTTON != 0

        convertResIdToStringInArgs("titleResId", "title")
        convertResIdToStringInArgs("messageResId", "message")
        convertResIdToStringInArgs("editTextResId", "editText")
        convertResIdToStringInArgs("posbtnTextResId", "posbtnText")
        convertResIdToStringInArgs("negbtnTextResId", "negbtnText")
        convertResIdToStringInArgs("ntrlbtnTextResId", "ntrlbtnText")

        //ここにButtonの処理など
        var view: View? = dialog.findViewById(ID_TITLE)

        // title
        if (view is AppCompatTextView) {
            if (!arguments!!.containsKey("title"))
                view.visibility = View.GONE
            else
                view.text = arguments!!.getString("title")
        }


        //edit_text
        view = dialog.findViewById(ID_EDIT_TEXT)
        if (view is AppCompatEditText) {
            if (!arguments!!.containsKey("editText")) view.visibility = View.GONE
            view.setText(arguments!!.getString("editText"))
        }

        // positive button
        view = dialog.findViewById(ID_POSITIVE_BUTTON)
        if (view != null) {
            if (noPosBtn)
                view.visibility = View.GONE
            else {
                view.setOnClickListener(this)
                if (view is TextView) {
                    val textView = view as TextView?
                    when {
                        arguments!!.containsKey("posbtnText") -> textView!!.text = arguments!!.getString("posbtnText")
                        noNegBtn -> textView!!.text = getString(android.R.string.ok)
                        else -> textView!!.text = getString(android.R.string.ok)
                    }
                }
            }
        }

        // negative button
        view = dialog.findViewById(ID_NEGATIVE_BUTTON)
        if (view != null) {
            if (noNegBtn)
                view.visibility = View.GONE
            else {
                view.setOnClickListener(this)
                if (view is TextView) {
                    val textView = view as TextView?
                    if (arguments!!.containsKey("negbtnText"))
                        textView!!.text = arguments!!.getString("negbtnText")
                    else
                        textView!!.text = getString(android.R.string.cancel)
                }
            }
        }
        LogFnc.logTraceEnd()
    }

    private fun convertResIdToStringInArgs(resIdkey: String, destKey: String) {

        val args = arguments
        if (isAdded) {
            if (args!!.containsKey(resIdkey)) {
                val string = getString(args.getInt(resIdkey))
                args.putString(destKey, string)
                args.remove(resIdkey)
            }
        }
    }

    override fun onResume() {
        LogFnc.logTraceStart()
        if (dismissFlag) { // dismiss a dialog
            val fragment = fragmentManager!!.findFragmentByTag(TAG)
            if (fragment is EditBottomDialog) {
                val dialogFragment = fragment as EditBottomDialog?
                dialogFragment!!.dismiss()
                fragmentManager!!.beginTransaction().remove(fragment).commit()
                dismissFlag = false
            }
        }
        super.onResume()
        LogFnc.logTraceEnd()
    }

    override fun onClick(v: View) {
        LogFnc.logTraceStart()
        if (clickGuard) return
        clickGuard = true
        val event = Event()
        event.eventId = -1
        val editText = dialog?.findViewById<AppCompatEditText>(ID_EDIT_TEXT)
        when (v.id) {
            ID_POSITIVE_BUTTON -> {

                if (position == 4 && TextUtils.isEmpty(editText!!.text!!.toString())) {
                    clickGuard = false
                    return
                }
                event.eventId = DialogFragmentListener.ON_POSITIVE_BUTTON_CLICKED
            }
            ID_NEGATIVE_BUTTON -> {
                event.eventId = DialogFragmentListener.ON_NEGATIVE_BUTTON_CLICKED
                if (position == 4 && TextUtils.isEmpty(editText!!.text!!.toString()))
                    event.eventId = DialogFragmentListener.ON_NEUTRAL_BUTTON_CLICKED
            }
        }
        event.text = editText!!.text!!.toString()
        event.position = position
        if (listener != null) listener!!.onEvent(listenerId, event)
        dismiss()
        LogFnc.logTraceEnd()
    }

    /**
     * Sets dialog title.
     *
     * @param title
     * dialog title
     * @return this [EditBottomDialog] instance
     */
    private fun setTitle(title: String): EditBottomDialog {

        arguments!!.putString("title", title)
        arguments!!.remove("titleResId")
        val dialog = dialog
        val view = dialog?.findViewById<View>(ID_TITLE)
        if (view is AppCompatTextView) view.text = title
        return this
    }

    /**
     * Sets dialog title.
     *
     * @param resId
     * resource id
     * @return this [EditBottomDialog] instance
     */
    fun setTitle(resId: Int): EditBottomDialog {

        if (isAdded)
            setTitle(getString(resId))
        else {
            arguments!!.putInt("titleResId", resId)
            arguments!!.remove("title")
        }
        return this
    }

    /**
     * Sets dialog message.
     *
     * @param message
     * dialog message
     * @return this [EditBottomDialog] instance
     */
    fun setEditText(message: String): EditBottomDialog {
        LogFnc.logTraceStart()
        arguments!!.putString("editText", message)
        arguments!!.remove("editTextResId")
        val view = dialog?.findViewById<View>(ID_EDIT_TEXT)
        LogFnc.logging(LogFnc.ERROR, message)
        if (view is AppCompatEditText) {
            view.setText(message)
            LogFnc.logging(LogFnc.ERROR, message)
        }
        LogFnc.logTraceEnd()
        return this
    }

    fun setListener(id: Int, listener: DialogFragmentListener): EditBottomDialog {

        this.listenerId = id
        this.listener = listener
        return this
    }

    fun setPosition(position: Int) {
        this.position = position
    }

    /**
     * Display the dialog, adding the fragment to the given FragmentManager.
     * This is a convenience for explicitly creating a transaction, adding the fragment to it with the given tag,
     * and committing it. This does *not* add the transaction to the back stack.
     * When the fragment is dismissed, a new transaction will be executed to remove it from the activity.
     *
     * @param manager
     * The FragmentManager this fragment will be added to.
     */
    fun show(manager: FragmentManager) {
        LogFnc.logTraceStart()
        deleteDialogFragment(manager)
        clickGuard = false

        // super.show(manager, TAG);
        val transaction = manager.beginTransaction()
        transaction.add(this, TAG)
        transaction.commitAllowingStateLoss()
        LogFnc.logTraceEnd()
    }

    /**
     * @param manager
     * [FragmentManager]
     */
    private fun deleteDialogFragment(manager: FragmentManager) {
        LogFnc.logTraceStart()
        val previous = manager.findFragmentByTag(TAG) as EditBottomDialog? ?: return

        val dialog = previous.dialog

        if (!dialog?.isShowing!!) return

        //        previous.onDismissExclusiveDialog();
        previous.dismiss()
        LogFnc.logTraceEnd()
    }

    companion object {
        // flags
        const val NO_POSITIVE_BUTTON = 1
        const val NO_NEGATIVE_BUTTON = 1 shl 1

        private const val LAYOUT = R.layout.dialog_content
        private const val ID_TITLE = R.id.dialog_title_textview
        private const val ID_EDIT_TEXT = R.id.dialog_edit_text
        private const val ID_POSITIVE_BUTTON = R.id.dialog_positive_button
        private const val ID_NEGATIVE_BUTTON = R.id.dialog_negative_button

        private const val TAG = "exclusive_dialog"

        fun newInstance(): EditBottomDialog {
            return newInstance("")
        }

        fun newInstance(text: String): EditBottomDialog {
            val args = Bundle()
            args.putString("editText",text)
            val editBottomDialog = EditBottomDialog()
            editBottomDialog.arguments = args
            return editBottomDialog
        }
    }
}
