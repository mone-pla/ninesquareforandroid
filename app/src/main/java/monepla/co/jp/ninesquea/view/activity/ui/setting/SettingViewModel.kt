package monepla.co.jp.ninesquea.view.activity.ui.setting

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthProvider
import monepla.co.jp.ninesquea.R
import monepla.co.jp.ninesquea.common.Common

class SettingViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is tools Fragment"
    }
    val text: LiveData<String> = _text

    fun getButtonName(context: Context): MutableLiveData<String> {
        return if(isAnonymous()) {
            MutableLiveData<String>().apply {
                value = "アカウント作成"
            }
        } else {
            MutableLiveData<String>().apply {
                value = context.getString(R.string.logout)
            }
        }
    }

    fun getButtonBuy(context: Context): MutableLiveData<String> {
        return MutableLiveData<String>().apply {
            value = if (Common.getSharePreferenceInt(context, Common.PREF_BUY, Common.PREF_BUY) == 1) context.getString(R.string.already_buy) else context.getString(R.string.app_billing)
        }
    }

    fun isAnonymous():Boolean {
        return FirebaseAuth.getInstance().currentUser!!.providerData.size == 1 && FirebaseAuth.getInstance().currentUser!!.providerData.any { data -> data.providerId == FirebaseAuthProvider.PROVIDER_ID }
    }

}