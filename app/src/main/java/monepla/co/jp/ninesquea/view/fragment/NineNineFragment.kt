package monepla.co.jp.ninesquea.view.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView

import java.io.File
import java.util.Objects

import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.gms.ads.MobileAds
import com.google.firebase.analytics.FirebaseAnalytics
import monepla.co.jp.ninesquea.common.Common
import monepla.co.jp.ninesquea.common.LogFnc
import monepla.co.jp.ninesquea.model.Detail
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.R
import monepla.co.jp.ninesquea.view.CellView
import monepla.co.jp.ninesquea.viewModel.NineNineViewModel

import monepla.co.jp.ninesquea.common.Common.ids
import monepla.co.jp.ninesquea.common.Common.llIds

/**
 * Created by user on 2017/01/09.
 *
 */

class NineNineFragment : Fragment() {
    private val cellViews = Array(9) {arrayOfNulls<CellView>(9)}

    private var nine: Nine? = null
    private var details: List<Detail>? = null
    private var title: String? = null
    @BindView(R.id.adView2)
    lateinit var mAdView: AdView
    private var viewModel: NineNineViewModel? = null
    private val args: NineNineFragmentArgs by this.navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_nine_nine, container, false)
        ButterKnife.bind(this, view)
        setHasOptionsMenu(true)
        MobileAds.initialize(context!!)
        val adRequest = AdRequest.Builder().build()
        if (Common.getSharePreferenceInt(activity!!, Common.PREF_BUY, Common.PREF_BUY) == 1) mAdView.visibility = View.GONE
        else mAdView.visibility = View.VISIBLE
        mAdView.loadAd(adRequest)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        /*
         * add( int groupId,
         *  int itemId,
         *  int order,
         *  int titleRes //title string or resource identifier )
         */
        menu.getItem(1).isVisible = true
        val item = menu.add(Menu.NONE, 1, Menu.NONE, R.string.download)
        item.setIcon(android.R.drawable.stat_sys_download)
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_home) {
            Navigation.findNavController(view!!).navigate(NineNineFragmentDirections.backHome())
            return super.onOptionsItemSelected(item)
        } else if(item.itemId == 1){
            LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.ADD_TO_CART,"Download")
            saveNineNine()
            return super.onOptionsItemSelected(item)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        LogFnc.logTraceStart()
        for (id in llIds) {
            Objects.requireNonNull<View>(view).findViewById<View>(id).findViewById<View>(R.id.adView).visibility = View.GONE
            for (tag in ids) view!!.findViewById<View>(id).findViewById<View>(tag).findViewById<View>(R.id.cell_button).visibility = View.GONE
        }

        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(activity!!.application).create(NineNineViewModel::class.java)
        cellViews[4][4] = view!!.findViewById<View>(llIds[4]).findViewById(ids[4])
        cellViews[4][4]!!.setTextSize(6)
        viewModel!!.getNine(args.nine).observe(this, Observer{ nine1 ->
            nine = nine1
            if (TextUtils.isEmpty(args.detail)) {
                cellViews[4][4]!!.text = nine!!.name!!
            }
            title = nine!!.name
        })

        viewModel!!.getDetailList(args.nine).observe(this, Observer{ details ->
            this.details = details
            for (detail in details.filter { d -> d.detailId == args.detail }) setLayout(detail)
        })

        if (!TextUtils.isEmpty(args.detail)) {
            viewModel!!.getDetail(args.detail).observe(this, Observer{ detail -> cellViews[4][4]!!.text = detail.name!! })
        }

        LogFnc.logTraceEnd()
    }

    private fun setLayout(detail: Detail) {
        val subView = view!!.findViewById<View>(llIds[detail.no])

        for (child in details!!.filter { d -> d.detailId == detail.objectId }) {
            cellViews[detail.no][child.no] = subView.findViewById(ids[child.no])
            cellViews[detail.no][child.no]!!.text = child.name!!
            cellViews[detail.no][child.no]!!.setTextSize(6)
        }

        cellViews[detail.no][4] = subView.findViewById(ids[4])
        cellViews[detail.no][4]!!.text = detail.name!!
        cellViews[detail.no][4]!!.setTextSize(6)
        cellViews[4][detail.no] = view!!.findViewById<View>(llIds[4]).findViewById(ids[detail.no])
        cellViews[4][detail.no]!!.text = detail.name!!
        cellViews[4][detail.no]!!.setTextSize(6)
    }


    /**
     * キャプチャを撮る
     * @param view view
     * @return 撮ったキャプチャ(Bitmap)
     */
    private fun getViewCapture(view: View): Bitmap {
        view.isDrawingCacheEnabled = true

        // Viewのキャッシュを取得
        val cache = view.drawingCache
        val screenShot = Bitmap.createBitmap(cache)
        view.isDrawingCacheEnabled = false
        return screenShot
    }


    private fun saveNineNine() {
        if (view == null || activity == null) return

        if (Common.checkPermission(activity!!)) {
            ActivityCompat.requestPermissions(activity!!,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1)
            return
        }
        export()
    }

    private fun export() {
        val capture = getViewCapture(view!!)
        // パスを指定してファイルを読み込む
        val file = File(Environment.getExternalStorageDirectory().toString() + "/" + title + System.nanoTime() + ".png")
        // 指定したファイル名が無ければ作成する。
        if (file.parentFile.mkdir()) LogFnc.logging(LogFnc.DEBUG, "ファイル作成")

        if (view == null) return
        if (Common.saveCapture(capture, file)) {
            Toast.makeText(context, R.string.success, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, R.string.fail, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Receive the result from a previous call to
     * [.startActivityForResult].  This follows the
     * related Activity API as described there in
     * [Activity.onActivityResult].
     *
     * @param requestCode The integer request code originally supplied to
     * startActivityForResult(), allowing you to identify who this
     * result came from.
     * @param resultCode The integer result code returned by the child activity
     * through its setResult().
     * @param data An Intent, which can return result data to the caller
     * (various data can be attached to Intent "extras").
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, results: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, results)
        if (requestCode != 1) return
        if (results.isNotEmpty() && results[0] == PackageManager.PERMISSION_GRANTED) {
            export()
        }
    }
}
