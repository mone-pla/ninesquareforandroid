package monepla.co.jp.ninesquea.common

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log

import com.google.firebase.analytics.FirebaseAnalytics

import java.text.SimpleDateFormat
import java.util.Locale


/**
 * Created by user on 2016/07/12.
 */
object LogFnc {
    const val INFO = "i"
    const val WARNING = "w"
    const val DEBUG = "d"
    const val ERROR = "e"
    private const val TAB = ","
    private val date = SimpleDateFormat("yyyy/MM/dd HH:mm:ss SSS", Locale.JAPAN)


    fun logging(status: String, msg: String) {
        val element = current()
        val txt = TAB + status +
                TAB + element.className +
                TAB + element.methodName +
                TAB + element.lineNumber +
                TAB + Build.DEVICE +
                TAB + Build.VERSION.SDK_INT +
                TAB + date.format(System.currentTimeMillis()) +
                TAB + msg
        when (status) {
            INFO -> Log.i(element.className, txt)
            WARNING -> Log.w(element.className, txt)
            DEBUG -> Log.d(element.className, txt)
            ERROR -> Log.e(element.className, txt)
            else -> {
            }
        }

    }

    fun logTraceStart() {
        val element = current()
        Log.i(element.className,
                TAB + INFO +
                        TAB + element.className +
                        TAB + element.methodName +
                        TAB + element.lineNumber +
                        TAB + Build.DEVICE +
                        TAB + Build.VERSION.SDK_INT +
                        TAB + date.format(System.currentTimeMillis()) +
                        TAB + "start")
    }

    fun logTraceEnd() {
        val element = current()
        Log.i(element.className,
                TAB + INFO +
                        TAB + element.className +
                        TAB + element.methodName +
                        TAB + element.lineNumber +
                        TAB + Build.DEVICE +
                        TAB + Build.VERSION.SDK_INT +
                        TAB + date.format(System.currentTimeMillis()) +
                        TAB + "end")
    }

    fun fireBaseAnalytics(context: Context?, event: String, msg: String) {
        if(context == null) return
        val element = current()
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, element.className)
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, element.methodName)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, msg)
        FirebaseAnalytics.getInstance(context).logEvent(event, bundle)
    }

    private fun current(): StackTraceElement {
        return Thread.currentThread().stackTrace[4]
    }


}
