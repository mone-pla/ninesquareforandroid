package monepla.co.jp.ninesquea.model

import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table

import monepla.co.jp.ninesquea.common.AppModel


/**
 * Created by user on 2016/12/31.
 */
@Table(name = "Nine")
class Nine : AppModel() {
    @Column(name = "name")
    var name: String? = null
}
