package monepla.co.jp.ninesquea.view.activity.ui.home

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.android.vending.billing.IInAppBillingService
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.firebase.analytics.FirebaseAnalytics
import monepla.co.jp.ninesquea.R
import monepla.co.jp.ninesquea.adapter.NineAdapter
import monepla.co.jp.ninesquea.common.Common
import monepla.co.jp.ninesquea.common.EditEvent
import monepla.co.jp.ninesquea.common.LogFnc
import monepla.co.jp.ninesquea.view.activity.ui.home.HomeFragmentDirections.nextNineFragment

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var nineAdapter: NineAdapter
    private var mService: IInAppBillingService? = null


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        setLayout(root)
        return root
    }

    /**
     * Called when the Fragment is visible to the user.  This is generally
     * tied to [Activity.onStart] of the containing
     * Activity's lifecycle.
     */
    override fun onStart() {
        super.onStart()
        homeViewModel.getNineList().observe(this, Observer {
            nines -> nineAdapter.setNineList(nines)
        })
        val serviceIntent = Intent("com.android.vending.billing.InAppBillingService.BIND")
        serviceIntent.setPackage("com.android.vending")
        activity!!.bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE)
        if(Common.getSharePreferenceInt(context!!,Common.PREF_BUY,Common.PREF_BUY) == 1) {
            view!!.findViewById<View>(R.id.adView).visibility = View.GONE
        }
    }

    /**
     * Called when the fragment is no longer in use.  This is called
     * after [.onStop] and before [.onDetach].
     */
    override fun onDestroy() {
        super.onDestroy()
        if (mService != null) {
            activity!!.unbindService(mServiceConn)
        }
    }

    private fun setLayout(root:View) {
        val recyclerView = root.findViewById<RecyclerView>(R.id.recycler_view)
        nineAdapter = NineAdapter(context!!,listener)
        recyclerView.adapter = nineAdapter
        root.findViewById<View>(R.id.fab).setOnClickListener {
            LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.ADD_TO_CART,"New")
            findNavController(it).navigate(nextNineFragment("",""))
        }
        MobileAds.initialize(context!!)
        val mAdView = root.findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
        if (Common.getSharePreferenceInt(activity!!, Common.PREF_BUY, Common.PREF_BUY) == 1)  mAdView!!.visibility = View.GONE
    }

    private var listener: EditEvent.EventClickListener = object : EditEvent.EventClickListener {
        override fun onClick(position: Int) {
            LogFnc.logTraceStart()
            LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.SELECT_CONTENT,"Item Select")
            try {
                findNavController().navigate(nextNineFragment(nineAdapter.getNine(position).objectId!!,""))
            } catch (e: IllegalStateException) {
                onClick(position)
            }

            LogFnc.logTraceEnd()
        }

        override fun onLongClick(position: Int) {
            LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.REMOVE_FROM_CART,"Delete")
            val builder = AlertDialog.Builder(activity!!)
                    .setTitle(getString(R.string.delete))
                    .setPositiveButton(getString(R.string.ok)) { dialogInterface, i ->
                        homeViewModel.delete(nineAdapter.getNine(position))
                        dialogInterface.dismiss()
                    }
                    .setNegativeButton(getString(android.R.string.cancel)) { dialogInterface, i -> dialogInterface.dismiss() }
            builder.create()
            builder.show()
        }
    }


    /**
     * サービス接続
     */
    private var mServiceConn = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
        }

        override fun onServiceConnected(name: ComponentName,
                                        service: IBinder) {
            mService = IInAppBillingService.Stub.asInterface(service)
            check()
        }
    }
    /**
     * アプリ内課金購入済みチェック
     */
    private fun check() {
        try {
            // 購入したものを確認する
            val ownedItems = mService!!.getPurchases(3, context!!.packageName, "inapp", null)

            val response = ownedItems.getInt("RESPONSE_CODE")
            if (response != 0) return
            val ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST")
            if (ownedSkus != null && ownedSkus[0] == "hidden_ad") {
                view!!.findViewById<View>(R.id.adView).visibility = View.GONE
                Common.setSharePreference(context!!, Common.PREF_BUY, Common.PREF_BUY, 1)
            } else {
                view!!.findViewById<View>(R.id.adView).visibility = View.VISIBLE
                Common.setSharePreference(context!!, Common.PREF_BUY, Common.PREF_BUY, 0)
            }


        } catch (e: Exception) {
            e.printStackTrace()

        }

    }
}