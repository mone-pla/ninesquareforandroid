package monepla.co.jp.ninesquea.common

import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import butterknife.BindView

import monepla.co.jp.ninesquea.R

abstract class BaseActivity : AppCompatActivity() {
    @BindView(R.id.progress_circular)
    lateinit var progressBar: ContentLoadingProgressBar

    protected fun progressShow() {
        progressBar.show()
    }

    protected fun progressHide() {
        progressBar.hide()
    }
}
