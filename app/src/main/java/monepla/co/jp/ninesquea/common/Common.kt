package monepla.co.jp.ninesquea.common

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap

import androidx.core.content.ContextCompat

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.UUID

import monepla.co.jp.ninesquea.R

import android.content.Context.MODE_PRIVATE

/**
 * Created by user on 2016/12/31.
 */

object Common {
    var PARAM_NINE_ID = "nine_id"
    var PARAM_DETAIL_ID = "sub_id"
    var PARAM_OBJ_ID = "obj_id"
    var PARAM_UUID = "uuid"
    var PREF_BUY = "BUY"
    var PREF_DARK_MODE = "DARK_MODE"
    var INTENT_ANONYMOUS = "Anonymous"

    var ids = arrayOf(

            R.id.nine1, R.id.nine2, R.id.nine3, R.id.nine4, R.id.nine5, R.id.nine6, R.id.nine7, R.id.nine8, R.id.nine9)

    var llIds = intArrayOf(R.id.ll1, R.id.ll2, R.id.ll3, R.id.ll4, R.id.ll5, R.id.ll6, R.id.ll7, R.id.ll8, R.id.ll9)

    fun checkPermission(activity: Activity): Boolean {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
    }

    /**
     * 撮ったキャプチャを保存
     * @param capture Bitmap
     * @param file File
     */
    fun saveCapture(capture: Bitmap, file: File): Boolean {
        var ret = false
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file, false)
            // 画像のフォーマットと画質と出力先を指定して保存
            capture.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.flush()
            ret = true
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (fos != null) {
                try {
                    fos.close()
                } catch (ie: IOException) {
                    ie.printStackTrace()
                }

            }
        }
        return ret
    }

    /**
     * 内部保存
     * @param context Context
     * @param key String
     * @param value Object
     */
    fun setSharePreference(context: Context, pref_key: String, key: String, value: Any) {
        val preferences = context.getSharedPreferences(pref_key, MODE_PRIVATE)
        val editor = preferences.edit()
        when (value) {
            is String -> editor.putString(key, value)
            is Int -> editor.putInt(key, value)
            is Boolean -> editor.putBoolean(key, value)
            is Float -> editor.putFloat(key, value)
            is Long -> editor.putLong(key, value)
        }
        editor.apply()
    }

    /**
     * 内部数値データ取得
     * @param context Context
     * @param pref_key String
     * @param value_key String
     */
    fun getSharePreferenceInt(context: Context, pref_key: String, value_key: String): Int {
        val preferences = context.getSharedPreferences(pref_key, MODE_PRIVATE)
        return preferences.getInt(value_key, 0)
    }

    /**
     * UUID生成
     * @param context Context
     */
    fun createUUID(context: Context) {
        val preferences = context.getSharedPreferences(PARAM_UUID, MODE_PRIVATE)
        val uuid = preferences.getString(PARAM_UUID, null)
        if (uuid == null) {
            setSharePreference(context, PARAM_UUID, PARAM_UUID, UUID.randomUUID().toString())
        }
    }
}
