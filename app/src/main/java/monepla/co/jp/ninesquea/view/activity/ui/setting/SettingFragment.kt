package monepla.co.jp.ninesquea.view.activity.ui.setting

import android.app.Activity
import android.app.PendingIntent
import android.content.*
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SwitchCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.vending.billing.IInAppBillingService
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterDialogBuilder
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterUtils
import monepla.co.jp.ninesquea.Billing.IabHelper
import monepla.co.jp.ninesquea.R
import monepla.co.jp.ninesquea.common.Common
import monepla.co.jp.ninesquea.common.Common.PREF_DARK_MODE
import monepla.co.jp.ninesquea.common.LogFnc
import monepla.co.jp.ninesquea.databinding.FragmentSettingBinding
import monepla.co.jp.ninesquea.view.activity.LoginActivity


class SettingFragment : Fragment() {
    private lateinit var binding:FragmentSettingBinding
    private var mService: IInAppBillingService? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = ViewModelProviders.of(this).get(SettingViewModel::class.java)
        binding.viewModel!!.getButtonName(context!!).observe(this, Observer { text -> binding.buttonName = text })
        binding.viewModel!!.getButtonBuy(context!!).observe(this, Observer { text -> binding.buttonNameBuy = text })
        binding.root.findViewById<AppCompatTextView>(R.id.logout).setOnClickListener {onClicked()}
        binding.root.findViewById<AppCompatTextView>(R.id.review).setOnClickListener { launchStore() }
        binding.root.findViewById<AppCompatTextView>(R.id.buy).setOnClickListener { buy() }
        return binding.root
    }

    private fun onClicked() {
        LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.LOGIN,"Logout")
        if(!binding.viewModel!!.isAnonymous())FirebaseAuth.getInstance().signOut()
        val intent = Intent(activity!!,LoginActivity::class.java)
        intent.putExtra(Common.INTENT_ANONYMOUS,binding.viewModel!!.isAnonymous())
        startActivity(intent)
        activity!!.finish()
    }

     private fun launchStore() {
         LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.LEVEL_START,"Review")
         // レビューダイアログを生成する
         val builder = AppiraterDialogBuilder(context!!)
         builder
                 // ダイアログのタイトル
                 .setTitle("このアプリを評価してください")
                 // ダイアログのメッセージ
                 .setMessage("このアプリの評価をお願いします！")
                 // ストアのアプリページに遷移するボタン
                 .addButton("★5をつける", fiveClick)
         val dialog = builder.create()
         dialog.show()
    }

    private var fiveClick :AppiraterDialogBuilder.OnClickListener = AppiraterDialogBuilder.OnClickListener { dialog ->
        AppiraterUtils.launchStore(context!!)
        dialog!!.dismiss()
    }

    /**
     * Called when the Fragment is visible to the user.  This is generally
     * tied to [Activity.onStart] of the containing
     * Activity's lifecycle.
     */
    override fun onStart() {
        super.onStart()
        val serviceIntent = Intent("com.android.vending.billing.InAppBillingService.BIND")
        serviceIntent.setPackage("com.android.vending")
        activity!!.bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE)
    }

    /**
     * Called when the fragment is no longer in use.  This is called
     * after [.onStop] and before [.onDetach].
     */
    override fun onDestroy() {
        super.onDestroy()
        if (mService != null) {
            activity!!.unbindService(mServiceConn)
        }
    }

    private var changeListener: CompoundButton.OnCheckedChangeListener = CompoundButton.OnCheckedChangeListener { _, checked ->
        setDefaultNightMode(if(checked)MODE_NIGHT_YES else MODE_NIGHT_NO)
        Common.setSharePreference(context!!,PREF_DARK_MODE, PREF_DARK_MODE,checked)
        activity!!.recreate()
    }

//    private fun setDarkMode() {
//        val items = arrayOf("システムデフォルト","ライトモード","ナイトモード")
//        val mode = Common.getSharePreferenceInt(context!!, PREF_DARK_MODE, PREF_DARK_MODE)
//        AlertDialog.Builder(context!!).setTitle(R.string.theme)
//                .setSingleChoiceItems(items,mode) { dialog, which ->
//                    when (which) {
//                        0 -> setDefaultNightMode(MODE_NIGHT_FOLLOW_SYSTEM)
//                        1 -> setDefaultNightMode(MODE_NIGHT_NO)
//                        else -> setDefaultNightMode(MODE_NIGHT_YES)
//                    }
//                    Common.setSharePreference(context!!, PREF_DARK_MODE, PREF_DARK_MODE,which)
//                    activity!!.recreate()
//                    dialog.dismiss()
//                }.setNegativeButton(R.string.cancel) { dialog, _ ->
//                    dialog.dismiss()
//                }.create().show()
//
//    }

    /**
     * アプリ内課金購入処理
     */
    @Throws(IntentSender.SendIntentException::class, RemoteException::class)
    private fun buy() {
        LogFnc.fireBaseAnalytics(context, FirebaseAnalytics.Event.ADD_TO_CART,"Buy")
        if (Common.getSharePreferenceInt(context!!, Common.PREF_BUY, Common.PREF_BUY) == 1) {
            Toast.makeText(context!!,R.string.already_buy,Toast.LENGTH_SHORT).show()
            return
        }
        // 購入リクエストの送信
        if (mService == null) {
            Toast.makeText(context!!, R.string.fail, Toast.LENGTH_SHORT).show()
            return
        }
        val buyIntentBundle = mService!!.getBuyIntent(3, context!!.packageName, "hidden_ad", "inapp", "add")
        // レスポンスコードを取得する
        val response = buyIntentBundle.getInt("RESPONSE_CODE")
        // 購入可能
        // BILLING_RESPONSE_RESULT_OK
        if (response != 0) return
        // 購入フローを開始する
        val pendingIntent = buyIntentBundle.getParcelable<PendingIntent>("BUY_INTENT")!!
        // 購入トランザクションの完了
        activity!!.startIntentSenderForResult(
                pendingIntent.intentSender,
                1001,
                Intent(),
                0,
                0,
                0)
    }

    /**
     * サービス接続
     */
    private var mServiceConn = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
        }

        override fun onServiceConnected(name: ComponentName,
                                        service: IBinder) {
            mService = IInAppBillingService.Stub.asInterface(service)
        }
    }


    // 購入結果をActivityが受け取るための設定
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // 返却コードを判別
        if (requestCode == 1001 && resultCode == Activity.RESULT_OK) {
            // 結果ステータスを取得
            val responseCode = data!!.getIntExtra("RESPONSE_CODE", 0)
            val purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA")
            // 成功
            if (responseCode == IabHelper.BILLING_RESPONSE_RESULT_OK && !TextUtils.isEmpty(purchaseData)) {
                Common.setSharePreference(context!!, Common.PREF_BUY, Common.PREF_BUY, 1)
                view!!.findViewById<View>(R.id.adView).visibility = View.GONE
                view!!.findViewById<View>(R.id.billing_text).visibility = View.GONE
            }
        } else {
            Toast.makeText(context!!, R.string.fail, Toast.LENGTH_SHORT).show()
        }
    }


}