package monepla.co.jp.ninesquea.view.activity.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import monepla.co.jp.ninesquea.model.Detail
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.repository.DetailRepository
import monepla.co.jp.ninesquea.repository.NineRepository
import java.util.ArrayList

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val nineRepository = NineRepository()
    private val detailRepository = DetailRepository()
    private var nineList = MutableLiveData<List<Nine>>()

    fun getNineList(): LiveData<List<Nine>> {
        nineRepository.collection.addSnapshotListener { documentSnapshots, e ->
            if (e != null || documentSnapshots == null) {
                nineList = MutableLiveData()
                return@addSnapshotListener
            }
            val list = ArrayList<Nine>()
            for (doc in documentSnapshots.documents) {
                val nine = doc.toObject(Nine::class.java)!!.withId<Nine>(doc.id)
                list.add(nine)
            }
            nineList.setValue(list)
        }

        return nineList
    }

    fun delete(nine: Nine): Task<Void> {
        return nineRepository.deleteItem(nine).addOnCompleteListener {
            detailRepository.collection.whereEqualTo("nineId",nine.objectId).get().addOnCompleteListener {task ->
                for (doc in task.result!!.documents) {
                    val detail = doc.toObject(Detail::class.java)!!.withId<Detail>(doc.id)
                    detailRepository.deleteItem(detail)
                }
            }
        }
    }
}