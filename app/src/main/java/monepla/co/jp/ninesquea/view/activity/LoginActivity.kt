package monepla.co.jp.ninesquea.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.ViewModelProvider

import com.activeandroid.query.Select
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth

import butterknife.BindView
import butterknife.ButterKnife
import com.activeandroid.query.Delete
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.EmailAuthProvider
import monepla.co.jp.ninesquea.common.BaseActivity
import monepla.co.jp.ninesquea.model.Nine
import monepla.co.jp.ninesquea.R
import monepla.co.jp.ninesquea.common.Common
import monepla.co.jp.ninesquea.common.LoadingListener
import monepla.co.jp.ninesquea.common.LogFnc
import monepla.co.jp.ninesquea.model.Detail
import monepla.co.jp.ninesquea.viewModel.AnyOneViewModel

class LoginActivity : BaseActivity() {

    private val auth = FirebaseAuth.getInstance()
    @BindView(R.id.fieldEmail)
    lateinit var emailEditText: AppCompatEditText
    @BindView(R.id.fieldPassword)
    lateinit var passwordEditText: AppCompatEditText
    @BindView(R.id.icon)
    lateinit var iconImagerView: AppCompatImageView

    private val isValidation: Boolean
        get() {
            var ret = false
            when {
                emailEditText.text!!.isEmpty() -> {
                    emailEditText.error = getString(R.string.err_non_email)
                    ret = true
                }
                !android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.text!!.toString()).matches() -> {
                    emailEditText.error = getString(R.string.err_format_email)
                    ret = true
                }
            }
            when {
                passwordEditText.text!!.length < 4 || passwordEditText.text!!.length > 32 -> {
                    passwordEditText.error = getString(R.string.err_length_password)
                    ret = true
                }
            }

            if (ret) progressHide()

            return ret
        }

    private val onCompleteListener = OnCompleteListener<AuthResult> { task ->
        progressHide()
        if (task.isSuccessful) {
            moveMain()
            return@OnCompleteListener
        }
        Toast.makeText(this, "", Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        val hyperspaceJump: Animation = AnimationUtils.loadAnimation(this, R.anim.hyperspace_jump)
        iconImagerView.startAnimation(hyperspaceJump)
    }

    override fun onStart() {
        super.onStart()
        LogFnc.fireBaseAnalytics(this,FirebaseAnalytics.Event.APP_OPEN,"start")
        when {
            intent.getBooleanExtra(Common.INTENT_ANONYMOUS,false) -> setLoginView()
            auth.currentUser != null -> moveMain()
            Select().from(Nine::class.java).exists() -> anyoneLogin()
            else -> setLoginView()
        }
    }

    private fun moveMain() {
        val intent = Intent(this, ListActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun upgradeChangeDB() {
        LogFnc.fireBaseAnalytics(this,FirebaseAnalytics.Event.GENERATE_LEAD,"upgradeChangeDB")
        val viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(AnyOneViewModel::class.java)
        val nineList = Select().from(Nine::class.java).execute<Nine>()

        viewModel.addNine(nineList, object : LoadingListener {
            override fun onCompleter() {
                Delete().from(Nine::class.java).execute<Nine>()
                Delete().from(Detail::class.java).execute<Nine>()
                this@LoginActivity.moveMain()
                progressHide()
            }
        })
    }

    private fun setLoginView() {
        findViewById<View>(R.id.login_layout).visibility = View.VISIBLE
        if(intent.getBooleanExtra(Common.INTENT_ANONYMOUS,false)) {
            findViewById<View>(R.id.emailSignInButton).visibility = View.GONE
        }
        findViewById<View>(R.id.emailCreateAccountButton).setOnClickListener {
            progressShow()
            if (isValidation) return@setOnClickListener
            if(intent.getBooleanExtra(Common.INTENT_ANONYMOUS,false)) {
                val credential = EmailAuthProvider.getCredential(emailEditText.text!!.toString(), passwordEditText.text!!.toString())
                auth.currentUser?.linkWithCredential(credential)
                        ?.addOnCompleteListener(onCompleteListener)
                return@setOnClickListener
            }
            LogFnc.fireBaseAnalytics(this,FirebaseAnalytics.Event.SIGN_UP,"SignUp")
            auth.createUserWithEmailAndPassword(emailEditText.text!!.toString(), passwordEditText.text!!.toString())
                    .addOnCompleteListener(onCompleteListener)
        }
        findViewById<View>(R.id.emailSignInButton).setOnClickListener {
            if (isValidation) return@setOnClickListener
            LogFnc.fireBaseAnalytics(this,FirebaseAnalytics.Event.LOGIN,"Login")
            auth.signInWithEmailAndPassword(emailEditText.text!!.toString(), passwordEditText.text!!.toString())
                    .addOnCompleteListener(onCompleteListener)

        }
        findViewById<View>(R.id.AnyOneButton).setOnClickListener { anyoneLogin() }
    }

    private fun anyoneLogin() {
        if(intent.getBooleanExtra(Common.INTENT_ANONYMOUS,false)) moveMain()
        auth.signInAnonymously().addOnCompleteListener { task ->
            LogFnc.fireBaseAnalytics(this,FirebaseAnalytics.Event.LOGIN,"anyone")
            if(task.isSuccessful) upgradeChangeDB()
            anyoneLogin()
        }
    }
}
