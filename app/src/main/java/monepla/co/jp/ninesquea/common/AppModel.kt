package monepla.co.jp.ninesquea.common


import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.activeandroid.Cache
import com.activeandroid.Model
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import java.io.Serializable
import java.util.*

/**
 * Created by user on 2017/04/01.
 */
@IgnoreExtraProperties
abstract class AppModel : Model(), Serializable {
    @Exclude
    var objectId: String? = null
        private set

    fun <T : AppModel> withId(objectId: String): T {
        this.objectId = objectId
        return this as T
    }

    fun saveModel(context: Context): Long? {
        // クラスのNCMBObjectを作成
        val mTableInfo = Cache.getTableInfo(javaClass)
        val preferences = context.getSharedPreferences(Common.PARAM_UUID, MODE_PRIVATE)
        var uuid = preferences.getString(Common.PARAM_UUID, null)
        if (uuid == null) {
            uuid = UUID.randomUUID().toString()
            preferences.edit().putString(Common.PARAM_UUID, uuid).apply()
        }
        val hashMap = HashMap<String, Any>()
        // オブジェクトの値を設定
        for (field in mTableInfo.fields) {
            val fieldName = mTableInfo.getColumnName(field)
            val fieldType = field.type

            field.isAccessible = true

            try {
                val value = field.get(this)

                if (value != null) {
                    if (fieldType == Byte::class.java || fieldType == Byte::class.javaPrimitiveType) {
                        hashMap[fieldName] = value
                    } else if (fieldType == Short::class.java || fieldType == Short::class.javaPrimitiveType) {
                        hashMap[fieldName] = value
                    } else if (fieldType == Int::class.java || fieldType == Int::class.javaPrimitiveType) {
                        hashMap[fieldName] = value
                    } else if (fieldType == Long::class.java || fieldType == Long::class.javaPrimitiveType) {
                        hashMap[fieldName] = value
                    } else if (fieldType == Float::class.java || fieldType == Float::class.javaPrimitiveType) {
                        hashMap[fieldName] = value
                    } else if (fieldType == Double::class.java || fieldType == Double::class.javaPrimitiveType) {
                        hashMap[fieldName] = value
                    } else if (fieldType == Boolean::class.java || fieldType == Boolean::class.javaPrimitiveType) {
                        hashMap[fieldName] = value
                    }
                }
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            }

        }
        for ((key, value) in hashMap) {
            if (key == "Id") continue

        }
        return save()
    }
}
