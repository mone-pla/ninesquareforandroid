package monepla.co.jp.ninesquea

import android.app.Application

import com.activeandroid.ActiveAndroid
import com.google.android.gms.ads.MobileAds

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        ActiveAndroid.initialize(this)
    }

    override fun onTerminate() {
        super.onTerminate()
        ActiveAndroid.dispose()
    }
}
