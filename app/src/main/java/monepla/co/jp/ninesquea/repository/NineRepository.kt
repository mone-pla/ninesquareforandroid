package monepla.co.jp.ninesquea.repository

import monepla.co.jp.ninesquea.common.FirebaseRepository
import monepla.co.jp.ninesquea.model.Nine

class NineRepository : FirebaseRepository<Nine>() {
    override val rootNode: String
        get() = "nine"

}
