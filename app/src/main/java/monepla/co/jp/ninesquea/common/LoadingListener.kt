package monepla.co.jp.ninesquea.common

interface LoadingListener {
    fun onCompleter()
}
