package monepla.co.jp.ninesquea.common

/**
 * Created by user on 2016/12/31.
 */

interface DialogFragmentListener {

    /**
     * On event.
     *
     * @param id
     * listener id
     * @param event
     * event is either of the following. [.ON_DISMISS] [.ON_CANCEL],
     * [.ON_POSITIVE_BUTTON_CLICKED], [.ON_NEGATIVE_BUTTON_CLICKED],
     * [.ON_CLOSE_BUTTON_CLICKED].
     */
    fun onEvent(id: Int, event: Event)

    companion object {
        val ON_DISMISS = 0
        val ON_CANCEL = 1
        val ON_POSITIVE_BUTTON_CLICKED = 2
        val ON_NEGATIVE_BUTTON_CLICKED = 3
        val ON_NEUTRAL_BUTTON_CLICKED = 4
        val ON_CLOSE_BUTTON_CLICKED = 5
    }
}
