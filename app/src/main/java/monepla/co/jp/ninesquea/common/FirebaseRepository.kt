package monepla.co.jp.ninesquea.common

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

abstract class FirebaseRepository<M : AppModel> {
    private val firestore = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser
    protected abstract val rootNode: String

    val collection: CollectionReference
        get() = firestore.collection("users").document(user!!.uid).collection(rootNode)

    fun getItem(id: String): DocumentReference {
        return collection.document(id)
    }

    fun createItem(model: M): Task<DocumentReference> {
        return collection.add(model)
    }

    fun updateItem(model: M): Task<Void> {
        return collection.document(model.objectId!!).set(model)
    }

    fun deleteItem(model: M): Task<Void> {
        return collection.document(model.objectId!!).delete()
    }

}
