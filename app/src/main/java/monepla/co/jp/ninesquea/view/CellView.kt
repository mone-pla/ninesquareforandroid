package monepla.co.jp.ninesquea.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout

import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import monepla.co.jp.ninesquea.R

/**
 * TODO: document your custom view class.
 */
class CellView : RelativeLayout {

    @BindView(R.id.nine_text)
    lateinit var textView: AppCompatTextView
    @BindView(R.id.cell_button)
    lateinit var button: AppCompatButton
    private var cellEventListener: CellEventListener? = null

    var text: String
        get() = textView.text.toString()
        set(text) {
            textView.text = text
        }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context) : super(context) {
        init(context)
    }

    private fun init(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.cell_view, this, true)
        ButterKnife.bind(this)
    }

    @OnClick(R.id.nine_text)
    fun textClick() {
        if (cellEventListener != null) cellEventListener!!.textClick(this)
    }

    fun setCellEventListener(cellEventListener: CellEventListener) {
        this.cellEventListener = cellEventListener
    }

    interface CellEventListener {
        fun textClick(v: View)
    }

    fun setButtonVisible(visible: Int) {
        button.visibility = visible
    }

    fun setTextSize(size: Int) {
        textView.textSize = size.toFloat()
    }
}
