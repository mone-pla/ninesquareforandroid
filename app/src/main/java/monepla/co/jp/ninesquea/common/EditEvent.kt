package monepla.co.jp.ninesquea.common

import android.view.View

/**
 * Created by user on 2016/12/31.
 */

class EditEvent(private val position: Int) : View.OnClickListener, View.OnLongClickListener {
    private var listener: EventClickListener? = null
    override fun onClick(view: View) {
        if (listener != null) listener!!.onClick(position)
    }

    override fun onLongClick(view: View): Boolean {
        if (listener != null) listener!!.onLongClick(position)
        return false
    }

    fun setListener(listener: EventClickListener) {
        this.listener = listener
    }

    interface EventClickListener {
        fun onClick(position: Int)
        fun onLongClick(position: Int)
    }
}
