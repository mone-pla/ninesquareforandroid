package monepla.co.jp.ninesquea.adapter

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView

import androidx.recyclerview.widget.RecyclerView

import monepla.co.jp.ninesquea.R


/**
 * Created by user on 2016/12/31.
 */

class NineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var nameTextView: TextView = itemView.findViewById(R.id.nine_name)
    var nineTextView: TextView = itemView.findViewById(R.id.nine_id)
    var linearLayout: CardView = itemView.findViewById(R.id.nine_linear_layout)

}
